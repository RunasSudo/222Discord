# 222Discord

A stylesheet for use with an app like BetterDiscord, or a browser addon like Stylish, to change the colours of Discord. By default uses #222 as a base, but is configurable at stylesheet build time.

![Screenshot](https://i.imgur.com/GyWGxhB.png)

## Licence

This theme is licensed under the GNU Affero General Public License version 3 or later.
